import { createApp } from 'vue';
import App from './App.vue';
//引入element-plus
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
//引入router
import router from './router';
//引入store
import store from './store';
//引入基本样式
import '@/style/index.scss';
//引入组件
import registerCustomComponents from './custom-components';
import registerGlobalComponent from '@/commom/registerGlobalComponent';

const app = createApp(App);
app.use(ElementPlus);
app.use(router);
app.use(store);
app.mount('#app');
//全局注册自定义组件
registerCustomComponents(app);
//全局组件注册
registerGlobalComponent(app);
