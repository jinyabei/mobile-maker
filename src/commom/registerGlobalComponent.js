import ActivityTopBarConfig from '@/views/activity/activityEdit/activityConfig/activityConfig';

const registerGlobalComponent = (Vue) => {
  Vue.component('ActivityConfig', ActivityTopBarConfig);
};

export default registerGlobalComponent;
