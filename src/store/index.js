import { getActivityTemplateData } from '@/commom';
import { createStore } from 'vuex';

export default createStore({
  state: {
    page: getActivityTemplateData(),
    currentComponent: {
      name: '', //组件名
      fullName: '', //全名
      mark: '', //组件标识
    },
  },
  getters: {
    page: (state) => state.page,
    currentComponent: (state) => state.currentComponent,
  },
  mutations: {
    addComponent(state, componentData) {
      state.page.components.push(componentData);
    },
    updateComponet(state, { newDetail, key }) {
      // key  1.page 更新活动detail 2.detail 更新组件detail 3.style 更新组件style
      if (key == 'page') {
        state.page.detail = { ...state.page.detail, ...newDetail };
        return;
      }
      const mark = state.currentComponent.mark;
      let currentComp = state.page.components.find((c) => c.mark === mark);
      key === 'style'
        ? (currentComp.style = { ...currentComp.style, ...newDetail })
        : (currentComp.detail = { ...currentComp.detail, ...newDetail });
    },
    //切换选中的组件
    changeCurrentComponent(state, newComp) {
      state.currentComponent = newComp;
    },
    //插入
    insertComponent(state, { i, componentData }) {
      // 数组任意位置插入组件
      state.page.components.splice(i, 0, componentData);
    },
    //删除
    deleteComponent(state, i) {
      // 提供下标，删除组件
      state.page.components.splice(i, 1);
    },
    //交换位置
    swapComponent(state, { orange: i1, target: i2 }) {
      // 提供组件下标，交换位置
      const components = state.page.components;
      [components[i1], components[i2]] = [components[i2], components[i1]];
    },
  },
  actions: {},
  modules: {},
});
