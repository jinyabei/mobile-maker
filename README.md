# mobile-maker
mobile-maker 是搭建一个低代码平台，用于快速制作h5页面。

该项目用于对低代码平台的学习和其中技术点的掌握。

## 项目组成和技术栈

配置服务（front): vue3 + element-plus + webpack
预览服务（preview): vue3 + element-plus + webpack
打包服务（mobile-maker): koa + shell + fs/child_process
接口服务（mobile-maker-server): koa + typescript + mysql

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Join
如果你对相关的内容也感觉兴趣的话，可与我探讨并完善该项目